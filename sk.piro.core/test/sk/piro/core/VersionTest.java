package sk.piro.core;

import junit.framework.TestCase;

import org.junit.Test;

/**
 * @author Roman Pipik, pipik.roman@gmail.com, roman.pipik@dominanz.sk
 */
public class VersionTest extends TestCase {

	Version v1 = new Version("1.1.1");
	Version v2 = new Version(1, 1, 1);
	Version v3 = new Version("1", "1", "1");
	Version v4 = new Version(v1, "1");
	Version v5 = new Version(1, 2, 3);

	@Test
	public void test() {
		assertEquals(v1, v2);
		assertEquals(v2, v3);

		assertNotSame(v3, v4);
		assertNotSame(v4, v5);
		assertNotSame(v1, v5);

		assertEquals(v2.toString(), v1.toString());

		assertTrue(v1.isLower(v5));
		assertTrue(v5.isHigher(v1));

		assertTrue(v1.isLower(v4));
		assertTrue(v4.isHigher(v1));
	}
}
