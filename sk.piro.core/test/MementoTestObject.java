import sk.piro.core.memento.Memento;
import sk.piro.core.memento.Mementoable;

/**
 * @author Roman Pipik, pipik.roman@gmail.com, roman.pipik@dominanz.sk
 */
public class MementoTestObject implements Memento<MementoTestObject> {
	public static final String s = "";

	@Override
	public MementoTestObject memento() {
		return this;
	}

	@Override
	public MementoTestObject clone() {
		return this;
	}
}

class MementoableTestObject implements Mementoable<MementoTestObject> {

	@Override
	public MementoTestObject memento() {
		return new MementoTestObject();
	}

}
