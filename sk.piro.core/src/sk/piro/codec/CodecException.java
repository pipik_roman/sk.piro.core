package sk.piro.codec;

/**
 * Exception in Codec
 * 
 * @author Roman Pipik, pipik.roman@gmail.com, roman.pipik@dominanz.sk
 */
public class CodecException extends RuntimeException {

	private final Object inputObject;

	/**
	 * Create codec exception with message and input object
	 * 
	 * @param message
	 *            exception message
	 * @param inputObject
	 *            object on input of codec
	 */
	public CodecException(String message, Object inputObject) {
		super(message);
		this.inputObject = inputObject;
	}

	/**
	 * Create codec exception with message , input object and cause
	 * 
	 * @param message
	 *            exception message
	 * @param inputObject
	 *            object on input of codec
	 * @param cause
	 *            cause of exception
	 */
	public CodecException(String message, Object inputObject, Throwable cause) {
		super(message, cause);
		this.inputObject = inputObject;
	}

	/**
	 * @return the inputObject
	 */
	public Object getInputObject() {
		return inputObject;
	}
}
