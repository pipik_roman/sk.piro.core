package sk.piro.codec;

import java.nio.charset.Charset;
import java.util.Arrays;

/**
 * Simple Base64 codec. Encode and decode as described in RFC 1521.
 */
public final class Base64Codec implements Codec<String, String> {
	private static final Charset UTF8 = Charset.forName("UTF-8");
	private static final char[] NIBBLE_TO_CHAR = new char[64];
	private static final byte[] CHAR_TO_NIBBLE = new byte[128];
	private static final Base64Codec INSTANCE = new Base64Codec();

	static {
		int i = 0;
		for (char c = 'A'; c <= 'Z'; c++) {
			NIBBLE_TO_CHAR[i++] = c;
		}
		for (char c = 'a'; c <= 'z'; c++) {
			NIBBLE_TO_CHAR[i++] = c;
		}
		for (char c = '0'; c <= '9'; c++) {
			NIBBLE_TO_CHAR[i++] = c;
		}
		NIBBLE_TO_CHAR[i++] = '+';
		NIBBLE_TO_CHAR[i++] = '/';

		byte minusOne = -1;
		Arrays.fill(CHAR_TO_NIBBLE, minusOne);
		for (byte j = 0; j < 64; j++) {
			CHAR_TO_NIBBLE[NIBBLE_TO_CHAR[j]] = j;
		}
	}

	/**
	 * Create codec
	 */
	private Base64Codec() {

	}

	/**
	 * Returns codec instance
	 * 
	 * @return the instance of codec
	 */
	public static Base64Codec getInstance() {
		return INSTANCE;
	}

	@Override
	public String decode(String encoded) {
		try {
			return new String(decodeAsBytes(encoded), UTF8);
		}
		catch (IllegalArgumentException e) {
			throw new CodecException("Decoding of object: " + encoded + " failed", encoded, e);
		}
	}

	@Override
	public String encode(String decoded) {
		try {
			return new String(encode(decoded.getBytes(UTF8)));
		}
		catch (IllegalArgumentException e) {
			throw new CodecException("Encoding of object: " + decoded + " failed", decoded, e);
		}
	}

	/**
	 * Encodes byte array into Base64 format
	 * 
	 * @param input
	 *            input
	 * @return encoded char array
	 */
	private char[] encode(byte[] input) {
		return encode(input, 0, input.length);
	}

	/**
	 * Encodes byte array
	 * 
	 * @param input
	 *            input
	 * @param offset
	 *            offset of first byte to process
	 * @param size
	 *            number of processed bytes
	 * @return encoded char array
	 */
	private char[] encode(byte[] input, int offset, int size) {
		int outputSize = (size * 4 + 2) / 3;
		int paddedOutputSize = ((size + 2) / 3) * 4;
		char[] out = new char[paddedOutputSize];
		int inputIndex = offset;
		int endIndex = offset + size;
		int outputIndex = 0;
		while (inputIndex < endIndex) {
			int i0 = input[inputIndex++] & 0xff;
			int i1 = inputIndex < endIndex ? input[inputIndex++] & 0xff : 0;
			int i2 = inputIndex < endIndex ? input[inputIndex++] & 0xff : 0;
			int o0 = i0 >>> 2;
			int o1 = ((i0 & 3) << 4) | (i1 >>> 4);
			int o2 = ((i1 & 0xf) << 2) | (i2 >>> 6);
			int o3 = i2 & 0x3F;
			out[outputIndex++] = NIBBLE_TO_CHAR[o0];
			out[outputIndex++] = NIBBLE_TO_CHAR[o1];
			out[outputIndex] = outputIndex < outputSize ? NIBBLE_TO_CHAR[o2] : '=';
			outputIndex++;
			out[outputIndex] = outputIndex < outputSize ? NIBBLE_TO_CHAR[o3] : '=';
			outputIndex++;
		}
		return out;
	}

	/**
	 * Decodes a string
	 * 
	 * @param input
	 *            input
	 * @return An array containing the decoded data bytes.
	 * @throws IllegalArgumentException
	 *             on decoding error
	 */
	private byte[] decodeAsBytes(String input) {
		return decode(input.toCharArray());
	}

	/**
	 * Decodes byte array
	 * 
	 * @param input
	 *            input
	 * @return decoded byte array
	 * @throws IllegalArgumentException
	 *             on decoding error
	 */
	private byte[] decode(char[] input) {
		return decode(input, 0, input.length);
	}

	/**
	 * Decode char array
	 * 
	 * @param input
	 *            input
	 * @param offset
	 *            offset of first char
	 * @param size
	 *            number of characters to process.
	 * @return decoded byte array
	 * @throws IllegalArgumentException
	 *             on decoding error
	 */
	private byte[] decode(char[] input, int offset, int size) {
		if (size % 4 != 0) { throw new IllegalArgumentException("Length of Base64 encoded input string is not a multiple of 4."); }
		while (size > 0 && input[offset + size - 1] == '=') {
			size--;
		}
		int outputSize = (size * 3) / 4;
		byte[] out = new byte[outputSize];
		int inputIndex = offset;
		int endIndex = offset + size;
		int outputIndex = 0;
		while (inputIndex < endIndex) {
			int i0 = input[inputIndex++];
			int i1 = input[inputIndex++];
			int i2 = inputIndex < endIndex ? input[inputIndex++] : 'A';
			int i3 = inputIndex < endIndex ? input[inputIndex++] : 'A';
			if (i0 > 127 || i1 > 127 || i2 > 127 || i3 > 127) { throw new IllegalArgumentException("Illegal Base64 encoded data."); }
			int b0 = CHAR_TO_NIBBLE[i0];
			int b1 = CHAR_TO_NIBBLE[i1];
			int b2 = CHAR_TO_NIBBLE[i2];
			int b3 = CHAR_TO_NIBBLE[i3];
			if (b0 < 0 || b1 < 0 || b2 < 0 || b3 < 0) { throw new IllegalArgumentException("Illegal Base64 encoded data."); }
			int o0 = (b0 << 2) | (b1 >>> 4);
			int o1 = ((b1 & 0xf) << 4) | (b2 >>> 2);
			int o2 = ((b2 & 3) << 6) | b3;
			out[outputIndex++] = (byte) o0;
			if (outputIndex < outputSize) out[outputIndex++] = (byte) o1;
			if (outputIndex < outputSize) out[outputIndex++] = (byte) o2;
		}
		return out;
	}
}
