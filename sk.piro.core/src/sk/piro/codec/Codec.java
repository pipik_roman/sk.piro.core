package sk.piro.codec;

/**
 * Codec template interface
 * 
 * @author Roman Pipik, pipik.roman@gmail.com, roman.pipik@dominanz.sk
 * @param <DecodedType>
 *            decoded type
 * @param <EncodedType>
 *            encoded type
 */
public interface Codec<DecodedType, EncodedType> {
	/**
	 * Decode encoded object
	 * 
	 * @param encoded
	 *            encoded object
	 * @return decoded object
	 * @throws CodecException
	 *             if decoding failed
	 */
	public DecodedType decode(EncodedType encoded) throws CodecException;

	/**
	 * Encode decoded object
	 * 
	 * @param decoded
	 *            decoded object
	 * @return encoded object
	 * @throws CodecException
	 *             if encoding failed
	 */
	public EncodedType encode(DecodedType decoded) throws CodecException;
}
