package sk.piro.core;

/**
 * Markup interface for strategy pattern
 * 
 * @author Roman Pipik, pipik.roman@gmail.com, roman.pipik@dominanz.sk
 */
public interface Strategy {
	// no methods
}
