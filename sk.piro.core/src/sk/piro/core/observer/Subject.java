package sk.piro.core.observer;

/**
 * Subject from Subject-Observer pattern
 * 
 * @author Roman Pipik, pipik.roman@gmail.com, roman.pipik@dominanz.sk
 * @param <ObserverType>
 *            type of observers - subclass of {@link Observer}
 */
public interface Subject<ObserverType extends Observer> {
	public ObserverRegistration register(ObserverType observer) throws InvalidObserverError;
}
