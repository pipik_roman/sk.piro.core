package sk.piro.core.observer;


/**
 * Exception thrown when subject doesn't support registered observer
 * 
 * @author Roman Pipik, pipik.roman@gmail.com, roman.pipik@dominanz.sk
 */
public class InvalidObserverError extends AssertionError {

	/**
	 * Create exception by specifying subject and observer
	 * 
	 * @param subject
	 *            subject that had to register observer
	 * @param observer
	 *            observer to register
	 */
	public InvalidObserverError(Subject<? extends Observer> subject, Observer observer) {
		super("Subject: " + subject + " cannot register observer: " + observer + " of type: " + observer.getClass() + "! Observer not supported.");
	}
}
