package sk.piro.core.observer;

/**
 * Observer from Subject-Observer pattern
 * 
 * @author Roman Pipik, pipik.roman@gmail.com, roman.pipik@dominanz.sk
 */
public interface Observer {
	// no methods, just marking interface
}
