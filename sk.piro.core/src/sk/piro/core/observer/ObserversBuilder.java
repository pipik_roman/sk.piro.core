package sk.piro.core.observer;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import sk.piro.core.Builder;

/**
 * Builder for easy creation of {@link ObserverRegistration} and easy creation of observer sets. Examples can be found in my other code (Actions API, ?...)
 * 
 * @author Roman Pipik, pipik.roman@gmail.com, roman.pipik@dominanz.sk
 * @param <ObserverType>
 *            type of observers used
 */
public class ObserversBuilder<ObserverType extends Observer> implements Builder<ObserverRegistration> {
	private final List<Set<ObserverType>> list = new ArrayList<Set<ObserverType>>();
	private final ObserverType observer;

	/**
	 * Create observer builder
	 * 
	 * @param observer
	 *            observer to add
	 */
	public ObserversBuilder(ObserverType observer) {
		this.observer = observer;
	}

	/**
	 * This method will add set of observers to created list of observers. If provided observers are null they are created and returned. At the end, observer will be added to each set
	 * 
	 * @param observers
	 *            observers to add, can be null for automatic creation of set
	 * @return provided set, or new set if provided set is null
	 */
	public Set<ObserverType> add(Set<ObserverType> observers) {
		if (observers == null) {
			observers = new LinkedHashSet<ObserverType>();
		}
		list.add(observers);
		return observers;
	}

	@Override
	public ObserverRegistration create() {
		for (Set<ObserverType> set : list) {
			set.add(observer);
		}
		return new ObserverRegistration() {
			@Override
			public void remove() {
				for (Set<ObserverType> set : list) {
					set.remove(observer);
				}
			}
		};
	}
}
