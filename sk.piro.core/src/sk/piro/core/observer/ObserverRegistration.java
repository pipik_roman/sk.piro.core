package sk.piro.core.observer;


/**
 * Registration of observer. Returned by registrator to secure removing of observer
 * 
 * @author Roman Pipik, pipik.roman@gmail.com, roman.pipik@dominanz.sk
 */
public interface ObserverRegistration {
	public void remove();
}
