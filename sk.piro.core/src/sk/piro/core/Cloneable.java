package sk.piro.core;

import sk.piro.core.memento.Memento;

/**
 * Interface for cloneable objects
 * 
 * @author Roman Pipik, pipik.roman@gmail.com, roman.pipik@dominanz.sk
 * @param <R>
 *            real type of object
 */
public interface Cloneable<R> {

	/**
	 * This will return clone of object. For {@link Memento} objects it can return same object instead of cloning
	 * 
	 * @return clone of object
	 */
	public R clone();
}
