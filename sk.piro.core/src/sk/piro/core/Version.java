package sk.piro.core;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Iterator;
import java.util.regex.Pattern;

import sk.piro.core.collections.ArrayIterator;

/**
 * Version. Parts of version are represented as strings (For now only numeric values supported, but later custom tags can be added (RC, alpha, ...))
 * 
 * @author Roman Pipik, pipik.roman@gmail.com, roman.pipik@dominanz.sk
 */
public class Version implements Serializable, Comparable<Version>, Iterable<String>, Immutable {

	public static final String NUMBER_REGEX = "[0-9]+";
	public static final Pattern NUMBER_PATTERN = Pattern.compile(NUMBER_REGEX);
	// public static final String RC_REGEX = NUMBER_REGEX + "-?RC" + NUMBER_REGEX;
	// public static final Pattern RC_PATTERN = Pattern.compile(RC_REGEX, Pattern.CASE_INSENSITIVE);

	/**
	 * This is implementation of part compare algorithm.
	 */
	public static final Comparator<String> PARTS_COMPARATOR = new Comparator<String>() {

		@Override
		public int compare(String o1, String o2) {
			try {
				int i1 = Integer.parseInt(o1);
				int i2 = Integer.parseInt(o2);
				if (i1 < i2) return -1;
				else if (i1 > i2) return 1;
				else return 0;
			}
			catch (NumberFormatException e) {
				throw new IllegalArgumentException("Cannot compare two version parts (Supporting only numeric values for now!) part1=" + o1 + ", part2=" + o2);
			}
		}
	};

	private static String[] createParts(int[] parts) {
		String[] strings = new String[parts.length];
		for (int i = 0; i < parts.length; i++) {
			strings[i] = Integer.toString(parts[i]);
		}
		return strings;
	}

	private static String[] createParts(String[] parts1, String[] parts2) {
		String[] allParts = new String[parts1.length + parts2.length];
		System.arraycopy(parts1, 0, allParts, 0, parts1.length);
		System.arraycopy(parts2, 0, allParts, parts1.length, parts2.length);
		return allParts;
	}

	/*
	 * End of static part -----------------------------------------------------
	 */

	private final String[] parts;

	/**
	 * Create version form string. String is split by dots!
	 * 
	 * @param version
	 *            version string
	 */
	public Version(String version) {
		this(version.split("\\."));
	}

	/**
	 * Create version from parts of version.
	 * 
	 * @param parts
	 *            parts of version
	 */
	public Version(String ... parts) {
		assert parts != null : "Version parts cannot be null!";
		assert parts.length > 0 : "At least one version part is required, but empty collection provided!";
		this.parts = parts;
	}

	public Version(int ... parts) {
		this(createParts(parts));
	}

	/**
	 * Create version combining version and subversion objects
	 * 
	 * @param version
	 *            version as string
	 * @param subversion
	 *            subversion
	 */
	public Version(String version, Version subversion) {
		this(createParts(version.split("\\."), subversion.parts));
	}

	/**
	 * Create version combining version and subversion objects
	 * 
	 * @param version
	 *            version
	 * @param subversion
	 *            subversion as string
	 */
	public Version(Version version, String subversion) {
		this(createParts(version.parts, subversion.split("\\.")));
	}

	/**
	 * Create version combining version and subversion objects
	 * 
	 * @param version
	 *            version
	 * @param subversion
	 *            subversion
	 */
	public Version(Version version, Version subversion) {
		this(createParts(version.parts, subversion.parts));
	}

	/**
	 * Returns part of version on specified index
	 * 
	 * @param index
	 *            index of part
	 * @return part string
	 */
	public String getPart(int index) {
		return this.parts[index];
	}

	/**
	 * Returns number of parts in version
	 * 
	 * @return number of parts in version
	 */
	public int getSize() {
		return this.parts.length;
	}

	/**
	 * Returns true if this version is newer (higher)
	 * 
	 * @param v
	 *            other version
	 * @return true if this version is newer (higher)
	 */
	public boolean isHigher(Version v) {
		return this.compareTo(v) > 0;
	}

	/**
	 * Returns true if this version is older (lower)
	 * 
	 * @param v
	 *            other version
	 * @return true if this version is older (lower)
	 */
	public boolean isLower(Version v) {
		return this.compareTo(v) < 0;
	}

	/**
	 * Compares this and other version
	 */
	@Override
	public int compareTo(Version o) {
		int depth = Math.min(getSize(), o.getSize());
		for (int i = 0; i < depth; i++) {
			int cmp = PARTS_COMPARATOR.compare(this.parts[i], o.parts[i]);
			if (cmp != 0) { return cmp; }
		}
		if (getSize() > o.getSize()) return 1;
		else if (getSize() < o.getSize()) return -1;
		else return 0;
	}

	@Override
	public Iterator<String> iterator() {
		return new ArrayIterator<String>(parts);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Arrays.hashCode(parts);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) { return true; }
		if (obj == null) { return false; }
		if (!(obj instanceof Version)) { return false; }
		Version other = (Version) obj;
		if (!Arrays.equals(parts, other.parts)) { return false; }
		return true;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < parts.length; i++) {
			if (i > 0) {
				sb.append('.');
			}
			sb.append(parts[i]);
		}
		return sb.toString();
	}
}
