package sk.piro.core.collections;

import java.util.Iterator;

/**
 * Implementation of iterable that has no elements
 * 
 * @author Roman Pipik, pipik.roman@gmail.com, roman.pipik@dominanz.sk
 * @param <Type>
 *            type of iterable elements
 */
public class EmptyIterable<Type> implements Iterable<Type> {

	@Override
	public Iterator<Type> iterator() {
		return new Iterator<Type>() {

			@Override
			public boolean hasNext() {
				return false;
			}

			@Override
			public Type next() {
				throw new AssertionError("This iterator represents non-iterable object and this method should be never called (hasNext() returned false)");
			}

			@Override
			public void remove() {
				throw new UnsupportedOperationException("Cannot remove any elements from non-iterable object");
			}
		};
	}
}
