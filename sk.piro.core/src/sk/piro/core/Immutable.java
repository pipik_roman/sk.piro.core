package sk.piro.core;

/**
 * Marking interface for immutable objects, object is immutable for external sources, but can be mutated by its internals
 * 
 * @author Roman Pipik, pipik.roman@gmail.com, roman.pipik@dominanz.sk
 */
public interface Immutable {
	//
}
