package sk.piro.core.memento;

/**
 * @author Roman Pipik, pipik.roman@gmail.com, roman.pipik@dominanz.sk
 * @param <R>
 *            real memento type
 */
public class AbstractMemento<R extends AbstractMemento<R>> implements Memento<R> {

	protected AbstractMemento() {

	}

	@SuppressWarnings("unchecked")
	@Override
	public R memento() {
		return (R) this;
	}

	@SuppressWarnings("unchecked")
	@Override
	public R clone() {
		return (R) this;
	}

}
