package sk.piro.core.memento;

import sk.piro.core.Cloneable;
import sk.piro.core.Immutable;

/**
 * Memento marks object that cannot be mutated in any way, so this object returns always same values. Because values will never change we can return same object
 * 
 * @author Roman Pipik, pipik.roman@gmail.com, roman.pipik@dominanz.sk
 * @param <MementoType>
 *            type of memento object
 */
public interface Memento<MementoType extends Memento<MementoType>> extends Mementoable<MementoType>, Immutable, Cloneable<MementoType> {
	//
}
