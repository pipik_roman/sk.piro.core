package sk.piro.core.memento;

/**
 * Memento pattern. Provides method to create constant state of object.
 * 
 * @author Roman Pipik, pipik.roman@gmail.com, roman.pipik@dominanz.sk
 * @param <MementoType>
 *            object that contains state that will never change
 */
public interface Mementoable<MementoType extends Memento<MementoType>> {

	/**
	 * Creates memento object in actual state. If this is instance of {@link Memento} itself it can return itself
	 * 
	 * @return memento object
	 */
	public MementoType memento();
}
