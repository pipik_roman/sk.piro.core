package sk.piro.core;

/**
 * Builder interface. This provides method {@link #create()} to finish creation process of some object.
 * 
 * @author Roman Pipik, pipik.roman@gmail.com, roman.pipik@dominanz.sk
 * @param <ResultType>
 *            type of result to be build
 */
public interface Builder<ResultType> {

	/**
	 * Create resulting object of builder.
	 * 
	 * @return resulting object of builder
	 */
	public ResultType create();
}
