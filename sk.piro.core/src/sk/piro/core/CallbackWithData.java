package sk.piro.core;

/**
 * Callback interface with data
 * 
 * @author Roman Pipik, pipik.roman@gmail.com, roman.pipik@dominanz.sk
 * @param <DataType>
 *            type of data in callback
 */
public interface CallbackWithData<DataType> {

	public void callback(DataType data);
}
