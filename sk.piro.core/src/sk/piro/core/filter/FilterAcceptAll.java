package sk.piro.core.filter;

/**
 * Always accepting filter
 * 
 * @author Roman Pipik, pipik.roman@gmail.com, roman.pipik@dominanz.sk
 * @param <Element>
 *            type of elements
 */
public class FilterAcceptAll<Element> implements Filter<Element> {

	@Override
	public boolean accept(Element element) {
		return true;
	}
}
