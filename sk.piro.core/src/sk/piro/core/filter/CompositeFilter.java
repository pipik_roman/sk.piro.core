package sk.piro.core.filter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import sk.piro.core.observer.ObserverRegistration;
import sk.piro.core.observer.Subject;

/**
 * Filter for composition of multiple filters. All filters registered must pass. Composite filter is implemented as {@link Subject}. Filter order is preserved so first filters should be fastest to
 * achieve effectivity of filtering!
 * 
 * @author Roman Pipik, pipik.roman@gmail.com, roman.pipik@dominanz.sk
 * @param <Element>
 *            Elements to filter
 */
public class CompositeFilter<Element> implements Filter<Element>, Subject<Filter<Element>> {
	private final List<Filter<Element>> filters;

	/**
	 * Use this constructor to create mutable Composite Filter. Filters can be added through {@link #register(Filter)} method
	 */
	public CompositeFilter() {
		this.filters = new ArrayList<Filter<Element>>();
	}

	/**
	 * Use this constructor to create immutable Composite Filter.
	 * 
	 * @param filters
	 *            filters to use
	 */
	public CompositeFilter(Iterable<Filter<Element>> filters) {
		List<Filter<Element>> list = new ArrayList<Filter<Element>>();
		for (Filter<Element> filter : filters) {
			list.add(filter);
		}
		this.filters = Collections.unmodifiableList(list);
	}

	/**
	 * Add filter to this composite filter
	 * 
	 * @param filter
	 *            filter to add
	 * @throws UnsupportedOperationException
	 *             if this composite filter is immutable and no filters can be added
	 */
	@Override
	public ObserverRegistration register(final Filter<Element> filter) throws UnsupportedOperationException {
		ObserverRegistration r = new ObserverRegistration() {
			@Override
			public void remove() {
				CompositeFilter.this.filters.remove(filter);
			}
		};
		this.filters.add(filter);
		return r;
	}

	@Override
	public boolean accept(Element element) {
		for (Filter<Element> filter : filters) {
			if (!filter.accept(element)) { return false; }
		}
		return true;
	}

}
