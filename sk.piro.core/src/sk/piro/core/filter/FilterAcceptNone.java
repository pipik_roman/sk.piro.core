package sk.piro.core.filter;

/**
 * Never accept filter
 * 
 * @author Roman Pipik, pipik.roman@gmail.com, roman.pipik@dominanz.sk
 * @param <Element>
 *            type of elements
 */
public class FilterAcceptNone<Element> implements Filter<Element> {

	@Override
	public boolean accept(Element element) {
		return false;
	}

}
