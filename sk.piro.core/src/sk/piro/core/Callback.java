package sk.piro.core;

/**
 * Callback interface, with no data
 * 
 * @author Roman Pipik, pipik.roman@gmail.com, roman.pipik@dominanz.sk
 */
public interface Callback {

	/**
	 * Primitive callback with no data
	 */
	public void callback();

}
