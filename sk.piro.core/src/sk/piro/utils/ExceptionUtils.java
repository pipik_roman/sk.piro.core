package sk.piro.utils;

import java.io.PrintWriter;
import java.io.StringWriter;

/**
 * Utilities for exceptions
 * 
 * @author Roman Pipik, pipik.roman@gmail.com, roman.pipik@dominanz.sk
 */
public class ExceptionUtils {
	/**
	 * Return string with throwable and its stack trace
	 * 
	 * @param t
	 *            throwable to convert to string
	 * @return created string
	 */
	public static final String toString(Throwable t) {
		assert t != null : "Throwable cannot be null for ExceptionUtils.toString(Throwable)";
		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw);
		t.printStackTrace(pw);
		pw.close();
		return sw.toString();
	}
}
