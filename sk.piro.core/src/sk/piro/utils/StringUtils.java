package sk.piro.utils;

/**
 * @author Roman Pipik, pipik.roman@gmail.com, roman.pipik@dominanz.sk
 */
public class StringUtils {

	/**
	 * Assert that string is not null and not empty with default message for assertion failure
	 * 
	 * @param string
	 *            string to assert
	 */
	public static void assertNotNullNotEmpty(String string) {
		assertNotNullNotEmpty(string, "String cannot be null or empty");
	}

	/**
	 * Assert that string is not null and not empty.
	 * 
	 * @param string
	 *            string to assert
	 * @param msg
	 *            message for assertion failure
	 */
	public static void assertNotNullNotEmpty(String string, String msg) {
		assert string != null && !string.isEmpty() : msg;
	}

	// TODO toString array - konverzia elementov v poli na pole stringov!
}
