package sk.piro.utils;

import sk.piro.codec.Base64Codec;

/**
 * @author Roman Pipik, pipik.roman@gmail.com, roman.pipik@dominanz.sk
 */
public class HttpUtils {
	/**
	 * Returns string representing basic HTTP authorization
	 * 
	 * @param name
	 *            name of user
	 * @param password
	 *            password of user
	 * @return string representing basic HTTP authorization
	 */
	public static String basicHttpAuth(String name, String password) {
		return Base64Codec.getInstance().encode(name + ":" + password);
	}
}
