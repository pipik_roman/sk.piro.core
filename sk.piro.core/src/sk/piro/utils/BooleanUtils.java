package sk.piro.utils;

/**
 * Boolean utilities
 * 
 * @author Roman Pipik, pipik.roman@gmail.com, roman.pipik@dominanz.sk
 */
public class BooleanUtils {

	/**
	 * Advanced boolean value parsing
	 * Returns true when logical meaning of text is true, like 'true','yes' and '1' are returning true
	 * 
	 * @param text
	 *            text to parse
	 * @return true when logical meaning of text is true, like 'true','yes' and '1' are returning true
	 */
	public static final boolean parseBoolean(String text) {
		text = text.trim().toLowerCase();
		return text.equals("true") || text.equals("1") || text.equals("y") || text.equals("yes");
	}
}
